(ns plf02.core)

(defn associative-1
  [a, b, c]
  (associative? [a b c]))

(defn associative-2
  [a, b]
  (associative? {:a a :b b}))

(defn associative-3
  [palabra]
  (associative? palabra))

(associative-1 1 2 3)
(associative-2 1 2)
(associative-3 "Hola")

(defn boolean-1
  []
  (boolean? nil))

(defn boolean-2
  [palabra]
  (boolean? (new Boolean palabra)))

(defn boolean-3
  [valor]
  (boolean? valor))

(boolean-1)
(boolean-2 "Hola")
(boolean-3 true)

(defn char-1
  [a]
  (char? a))

(defn char-2
  [palabra]
  (char? palabra))

(defn char-3
  [palabra]
  (char? (first palabra)))

(char-1 3)
(char-2 "a")
(char-3 "avbc")

(defn coll-1
  []
  (coll? []))

(defn coll-2
  [a b c]
  (coll? [a b c]))

(defn coll-3
  [a b]
  (coll? {:a a :b b}))

(coll-1)
(coll-2 1 2 3)
(coll-3 2 1)

(defn decimal-1
  [a]
  (decimal? a))

(decimal-1 1)
(decimal-1 9M)
(decimal-1 10.6)

(defn double-1
  [a]
  (double? a))

(defn double-2
  [palabra]
  (double? (new Double palabra)))

(defn double-3
  [n]
  (double? (new BigDecimal n)))

(double-1 8)
(double-2 "1")
(double-3 "10")

(defn float-1
  [a]
  (float? a))

(float-1 0)
(float-1 10M)
(float-1 0.5)

(defn ident-1
  []
  (ident? :a))

(defn ident-2
  [a]
  (ident? a))

(defn ident-3
  [palabra]
  (ident? palabra))

(ident-1)
(ident-2 2)
(ident-3 "Hi")

(defn int-1
  [a]
  (int? a))

(defn int-2
  [a]
  (int? (bigint a)))

(defn int-3
  []
  (int? java.math.BigInteger/ONE))

(int-1 9)
(int-2 5)
(int-3)

(defn integer-1
  [a]
  (integer? a))

(defn integer-2
  []
  integer? (inc Integer/MAX_VALUE))

(integer-1 9)
(integer-1 5.3)
(integer-2)

(defn keyword-1
  []
  (keyword? :+))

(defn keyword-2
  []
  (keyword? true))

(keyword-1)
(keyword-2)

(defn list-1
  []
  (list? []))

(defn list-2
  [a]
  (list? a))

(defn list-3
  [a]
  (list? (range a)))

(list-1)
(list-2 0)
(list-3 10)

(defn map-entry-1
  [a b]
  (map-entry? (first {:a a :b b})))

(defn map-entry-2
  [a b]
  (class {:a a :b b}))

(defn map-entry-3
  [a b]
  (class (first {:b a :c b})))

(map-entry-1 1 2)
(map-entry-2 1 2)
(map-entry-3 1 2)

(defn map-1
  [a b]
  (map? {:a a :b b}))

(defn map-2
  [a b c]
  (map? (hash-map :a a :b b :c c)))

(defn map-3
  [a b]
  (map? (array-map :a a :b b)))

(map-1 1 2)
(map-2 1 2 3)
(map-3 1 2)

(defn nat-int-1
  [a]
  (nat-int? a))

(nat-int-1 8)
(nat-int-1 8N)
(nat-int-1 -7)

(defn number-1
  [a]
  (number? a))

(defn number-2
  []
  (number? nil))

(defn number-3
  [palabra]
  (number? palabra))

(number-1 108)
(number-2)
(number-3 "Jose")

(defn pos-int-1
  [a]
  (pos-int? a))

(defn pos-int-2
  [-a]
  (pos-int? -a))

(pos-int-1 10)
(pos-int-2 -10)

(defn ratio-1
  [a]
  (ratio? a))

(ratio-1 10)
(ratio-1 10/20)
(ratio-1 3.7)

(defn rational-1
  [a]
  (rational? a))

(rational-1 10)
(rational-1 4.5)
(rational-1 22/8)

(defn seq-1
  [a]
  (seq? a))

(defn seq-2
  [a]
  (seq? [a]))

(defn seq-3
  [a]
  (seq? (seq [a])))

(seq-1 2)
(seq-2 5)
(seq-3 9)

(defn seqable-1
  []
  (seqable? nil))

(defn seqable-2
  []
  (seqable? []))

(defn seqable-3
  [a]
  (seqable? a))

(seqable-1)
(seqable-2)
(seqable-3 6)

(defn sequential-1
  [a b c]
  (sequential? [a b c]))

(defn sequential-2
  [a b]
  (sequential? (range a b)))

(defn sequential-3
  [a]
  (sequential? a))

(sequential-1 8 9 10)
(sequential-2 3 9)
(sequential-3 10)

(defn set-1
  [a b c]
  (set? [a b c]))

(defn set-2
  [a b]
  (set? {:a a :b b}))

(defn set-3
  [a b c]
  (set? #{a b c}))

(set-1 1 2 3)
(set-2 1 2)
(set-3 1 2 3)

(defn some-1
  [a]
  (some? a))

(defn some-2
  []
  (some? nil))

(defn some-3
  []
  (some? []))

(some-1 10)
(some-2)
(some-3)

(defn string-1
  [a]
  (string? a))

(defn string-2
  [palabra]
  (string? palabra))

(defn string-3
  []
  (string? nil))

(string-1 10)
(string-2 "Pepe")
(string-3)

(defn qualified-symbol-1
  [a]
  (qualified-symbol? a))

(defn qualified-symbol-2
  [palabra]
  (qualified-symbol? palabra))

(defn qualified-symbol-3
  []
  (qualified-symbol? nil))

(qualified-symbol-1 60)
(qualified-symbol-2 "Yo")
(qualified-symbol-3)

(defn vector-1
  [a b c]
  (vector? [a b c]))

(defn vector-2
  [a b c]
  (vector? {:a a :b b :c c}))

(defn vector-3
  [a b c]
  (vector? (first {:a a :b b :c c})))

(vector-1 1 2 3)
(vector-2 1 2 3)
(vector-3 1 2 3)

(defn drop-1
  [a b c d]
  (drop 0 [a b c d]))

(defn drop-2
  [a b c d]
  (drop  b [a b c d]))

(defn drop-3
  [a b c d e]
  (drop e [a b c d]))

(drop-1 1 2 3 4)
(drop-2 1 2 3 4)
(drop-3 1 2 3 4 5)

(defn drop-last-1
  [a b c d]
  (drop-last [a b c d]))

(defn drop-last-2
  [a b c d]
  (drop-last 0 [a b c d]))

(defn drop-last-3
  [a b c d]
  (drop-last b (vector a b c d)))

(drop-last-1 1 2 3 4)
(drop-last-2 1 2 3 4)
(drop-last-3 1 2 3 4)

(defn drop-while-1
  [a b c d e f g h i j k l m n o p q r]
  (drop-while neg? [a b c d e f g h i j k l m n o p q r]))

(defn drop-while-2
  []
  (drop-while #(> 3 %) [1 2 3 4 5]))

(defn drop-while-3
  []
  (drop-while #(>= 3 %) [1 2 3 4 5]))

(drop-while-1 -1 -2 -3 -6 -7 1 2 3 4 -5 -6 0 1 4 9 -6 3 0)
(drop-while-2)
(drop-while-3)

(defn every-1
  [a b]
  (every? {a  "one" b "two"} [a b]))

(defn every-2
  []
  (every? even? '(2 4 6)))

(defn every-3
  [a b]
  (every? #{a b} [a b]))

(every-1 1 2)
(every-2)
(every-3 1 2)

(defn filterv-1
  [a]
  (filterv even? (range a)))

(defn filterv-2
  [a]
  (filterv (fn [x]
             (= (count x) a))
           ["a" "aa" "b" "n" "f" "lisp" "clojure" "q" ""]))

(defn filterv-3
  [a]
  (filterv #(= (count %) a)
           ["a" "aa" "b" "n" "f" "lisp" "clojure" "q" ""]))

(filterv-1 9)
(filterv-2 1)
(filterv-3 1)

(defn group-by-1
  [a]
  (group-by odd? (range a)))

(defn group-by-2
  [a b c]
  (group-by :user-id [{:user-id a :uri "/"}
                      {:user-id b :uri "/foo"}
                      {:user-id c :uri "/account"}]))

(defn group-by-3
  [a b c]
  (group-by :category [{:category "a" :id a}
                       {:category "a" :id b}
                       {:category "b" :id c}]))

(group-by-1 10)
(group-by-2 1 2 3)
(group-by-3 1 2 3)

(defn iterate-1
  [a]
  ((iterate inc a)))

(defn iterate-2
  [a]
  (take 5 (iterate inc a)))

(defn iterate-3
  [a b]
  (take a (iterate (partial + b) 0)))

(iterate-1 5)
(iterate-2 5)
(iterate-3 20 2)

(defn keep-1
  [a b]
  (keep even? (range a b)))

(defn keep-2
  [a b c]
  (keep {:a a, :b b, :c c} [:a :b :d]))

(defn keep-3
  [a b]
  (keep #(when (number? %) %) [a "a" b "c"]))

(keep-1 1 10)
(keep-2 1 2 3)
(keep-3 1 2)

(defn keep-indexed-1
  [a b c d e]
  (let [predicate #(= 1 %)
        sequence [a b c d e]]
    (first (keep-indexed (fn [i x] (when (predicate x) i))
                         sequence))))

(defn keep-indexed-2
  [a b c d e f g]
  (let [predicate #(= 1 %)
        sequence [a b c d e f g]]
    (first (keep-indexed (fn [i x] (when (predicate x) i))
                         sequence))))

(keep-indexed-1 3 2 4 1 5)
(keep-indexed-2 3 2 4 1 5 6 7)

(defn map-indexed-1
  [a]
  (map-indexed a))

(defn map-indexed-2
  [a b]
  (map-indexed a b))

(defn map-indexed-3
  []
  (map-indexed list [:a :b :c]))

(map-indexed-1 5)
(map-indexed-2 5 10)
(map-indexed-3)

(defn mapcat-1
  [a b]
  (mapcat #(repeat 2 %) [a b]))

(defn mapcat-2
  [a b c]
  (mapcat #(remove even? %) [[a b] [b b] [b c]]))

(defn mapcat-3
  [a b c d]
  (mapcat (juxt inc dec)  [a b c d]))

(mapcat-1 1 2)
(mapcat-2 1 2 3)
(mapcat-3 1 2 3 4)

(defn mapv-1
  [a b c d e]
  (mapv inc [a b c d e]))

(defn mapv-3
  [a b c]
  (mapv + [a b c] (iterate inc 1)))

(mapv-1 1 2 3 4 5)
(mapv-3 1 2 3)

(defn merge-with-1
  [a b]
  (merge-with merge {:x {:y a}} {:x {:z b}}))

(defn merge-with-2
  [a b c d e]
  (merge-with + {:a a  :b b} {:a c  :b d :c e}))

(defn merge-with-3
  [a b c]
  (merge-with + {:a a} {:a b} {:a c}))

(merge-with-1 1 2)
(merge-with-2 1 2 9 98 0)
(merge-with-3 1 2 3)

(defn not-any-1
  []
  (not-any? odd? '(2 4 6)))

(defn not-any-2
  []
  (not-any? nil? [true false false]))

(defn not-any-3
  []
  (not-any? nil? [true false nil]))

(not-any-1)
(not-any-2)
(not-any-3)

(defn not-every-1
  []
  (not-every? odd? '(1 2 3)))

(defn not-every-2
  []
  (not-every? odd? '(1 3)))

(not-every-1)
(not-every-2)

(defn partition-by-1
  [a b c d e]
  (partition-by #(= 3 %) [a b c d e]))

(defn partition-by-2
  [a b c]
  (partition-by even? [a a a b b c c]))

(defn partition-by-3
  [a b c]
  (partition-by identity [a a a a b b c]))

(partition-by-1 1 2 3 4 5)
(partition-by-2 1 2 3)
(partition-by-3 1 2 3)

(defn reduce-kv-1
  [a b c]
  (reduce-kv #(assoc %1 %3 %2) {} {:a a :b b :c c}))

(defn reduce-kv-2
  [palabra palabra2 palabra3]
  (reduce-kv (fn [res idx itm] (assoc res idx itm)) {}
             [palabra palabra2 palabra3]))

(reduce-kv-1 1 2 3)
(reduce-kv-2 "unos" "dos" "tres")

(defn remove-1
  [a b c d e]
  (remove pos? [a b c d e]))

(defn remove-2
  [a b c]
  (remove nil? [a nil b nil c nil]))

(defn remove-3
  [a]
  (remove even? (range a)))

(remove-1 -6 -2 1 -10 9)
(remove-2 10 14 8)
(remove-3 20)

(defn reverse-1
  [a b c]
  (reverse [a b c]))

(defn reverse-2
  [palabra]
  (reverse palabra))

(defn reverse-3
  [palabra]
  (apply str (reverse palabra)))

(reverse-1 1 2 3)
(reverse-2 "Hola")
(reverse-3 "Jose")

(defn some-a
  [a b c d]
  (some even? [a b c d]))

(defn some-b
  [a b c d e f]
  (some #(= 5 %) [a b c d e f]))

(defn some-c
  [a b c d]
  (some #(when (even? %) %) [a b c d]))

(some-a 1 2 3 4)
(some-b 1 2 3 4 5 6)
(some-c 1 2 3 4)

(defn sort-by-1
  [palabra palabra2 palabra3]
  (sort-by count [palabra palabra2 palabra3]))

(defn sort-by-2
  [a b c]
  (sort-by first [[a b] [b b] [b c]]))

(defn sort-by-3
  [a b c]
  (sort-by first > [[a b] [b b] [b c]]))

(sort-by-1 "Jose" "Yo" "Mundo")
(sort-by-2 1 2 3)
(sort-by-3 1 2 3)

(defn split-with-1
  [a b c d e]
  (split-with (partial >= 3) [a b c d e]))

(defn split-with-2
  [a b c]
  (split-with (partial > 3) [a b c b a]))

(defn split-with-3
  [a b c]
  (split-with (partial > 10) [a b c b a]))

(split-with-1 1 2 3 4 5)
(split-with-2 1 2 3)
(split-with-3 1 2 3)

(defn take-1
  [a b c d e f]
  (take 3 [a b c d e f]))

(defn take-2
  [a b c d e]
  (take 2 [a b c d e]))

(defn take-3
  [a b c d e f]
  (take 0 [a b c d e f]))

(take-1 1 2 3 4 5 6)
(take-2 1 2 3 4 5)
(take-3 1 2 3 4 5 6)

(defn take-last-1
  [a b c d e f]
  (take-last 3 [a b c d e f]))

(defn take-last-2
  [a b c d e]
  (take-last 2 [a b c d e]))

(defn take-last-3
  [a b c d e f]
  (take-last 0 [a b c d e f]))

(take-last-1 1 2 3 4 5 6)
(take-last-2 1 2 3 4 5)
(take-last-3 1 2 3 4 5 6)

(defn take-nth-1
  [a]
  (take-nth 2 (range a)))

(defn take-nth-2
  [a]
  (take 3 (take-nth 0 (range a))))

(defn take-nth-3
  [a]
  (take-nth 3 (range a)))

(take-nth-1 10)
(take-nth-2 2)
(take-nth-3 10)


(defn take-while-1
  [a]
  (reduce + (take-while (partial > a) (iterate inc 0))))

(defn take-while-2
  [a b c d e f]
  (take-while neg? [a b c d e f]))

(defn take-while-3
  [a b c d]
  (take-while #{[a b] [c d]} #{[a b]}))

(take-while-1 100)
(take-while-2 -3 -2 1 -1 2 5)
(take-while-3 1 2 3 4)

(defn nombre [] {:name "Jose" :age 18 :carrera "Sistemas"})

(defn update-1
  [a]
  (update nombre :age a))

(defn update-2
  [palabra]
  (update nombre :nombre palabra))

(defn update-3
  [palabra]
  (update nombre :carrera palabra))

(update-1 22)
(update-2 "Alberto")
(update-3 "Ingenieria")

(defn updatein-1
  [a]
  (update-in nombre [:age] a))

(defn updatein-2
  [nombre]
  (update nombre [:nombre] nombre))

(defn updatein-3
  [palabra]
  (update-in nombre [:carrera] palabra))